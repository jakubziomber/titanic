import pandas as pd

def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titanic_dataframe()
    
    # Step 1: Extract titles from the 'Name' column
    df['Title'] = df['Name'].str.extract(r'([A-Za-z]+)\.')

    # Create a dictionary to store median ages for each title
    median_ages = {}
    
    # Step 2: Calculate median ages for each title group
    for title in ["Mr.", "Mrs.", "Miss."]:
        median_age = df[df['Title'] == title]['Age'].median()
        median_ages[title] = median_age

    # Step 3: Fill missing age values with the respective median values
    for title, median_age in median_ages.items():
        df.loc[(df['Title'] == title) & df['Age'].isnull(), 'Age'] = median_age

    # Drop the 'Title' column if you don't need it in the final result
    df.drop('Title', axis=1, inplace=True)

    # Prepare the result in the requested format
    result = [(title, int(median_ages[title]), df[df['Title'] == title]['Age'].count()) for title in ["Mr.", "Mrs.", "Miss."]]

    return result